# *GoNews*  -- Golang每日新闻可视化浏览与检索平台


## 介绍

gonews是基于`go+vue`实现的golang每日新闻浏览与检索平台

- 项目地址：[Github](https://github.com/idoubi/gonews)
- 线上Demo：[GoNews](http://gonews.idoubi.cc)
- 数据来源：[GoCN每日新闻](https://github.com/gocn/news)

## 项目截图

![gonews](http://blogcdn.idoustudio.com/gonews-1.png)

## 部署


- 获取新闻数据

```
git clone https://github.com/gocn/news /data/news
```

- 获取源码

```
go get -u github.com/idoubi/gonews
```

- 启动后台服务，拉取、解析数据

```
nohup gonews -d /data/news > /data/log/gonews.log 2>&1 

./news -d ~/data/news -p 8017
```

查找进程以及关闭
```
 ps -ef | grep news
 kill -9 44286
```

可选参数：-pull <minutes> ，拉取数据源的时间间隔，单位分钟，默认值60

- 启动Api

```
nohup gonews -a api -p 8017 > /data/log/gonews.log 2>&1 &
```

```
nohup ./news.exe -d f:/gocn/news > f:/gocn/log/gonews.log 2>&1 &
nohup ./news.exe -a api -p 8017 f:/gocn/news -d f:/gocn/news > f:/gocn/log/gonews.log 2>&1 &
```

- 前端部署

```
cd $GOPATH/src/github.com/idoubi/gonews/web
npm install
npm run build
```

- Nginx配置

```
server {
    listen       80;
    server_name gonews.cc;
    index index.html index.htm index.php;
    root  /data/go/src/idoubi/gonews/web;

    location /api {
        rewrite         ^.+api/?(.*)$ /$1 break;
        proxy_pass      http://127.0.0.1:8017;
    }

    location /news {
        rewrite         ^.+news/?(.*)$ /$1 break;
        proxy_pass      http://gonews.cc;
    }
}
```
- 测试
* 测试单个文件，一定要带上被测试的原文件
    go test -v  wechat_test.go wechat.go 

* 测试单个方法
    go test -v -test.run TestRefreshAccessToken

## 用到的技术

### golang包

- github.com/go-redis/redis
- encoding/json
- flag
- net/http
- net/url
- strconv
- sync
- crypto/md5
- fmt
- io
- io/ioutil
- net/url
- os
- path/filepath
- regexp
- strconv
- strings
- time

### npm包

- vue
- vuex
- vue-router
- axios
- moment
- mockjs


欢迎提交Pull Request


